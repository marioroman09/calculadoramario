package ventana;

import static org.junit.Assert.*;

import org.junit.Test;

public class CalculadoraTest {

	@Test
	public void testSuma() {
		int resultado=Calculos.Suma(5,5);
		assertEquals(10,resultado);
	}
	
	@Test
	public void testResta() {
		int resultado=Calculos.Resta(5,2);
		assertEquals(3,resultado);
	}
	
	@Test
	public void testMult() {
		int resultado=Calculos.mult(3,3);
		assertEquals(9,resultado);
	}
	
	@Test
	public void testDiv() {
		int resultado=Calculos.div(6,2);
		assertEquals(3,resultado);
	}

}
