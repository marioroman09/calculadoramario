package ventana;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

	public class Ventana extends JFrame {
		

		public Ventana() {
			
			this.setSize(500,500); //Establecemos tamaño de ventana
			
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);//Para cerrar el programa al cerrar la ventana.
			
			setTitle("Calculadora Mario Román");//Titulo
			
			setLocationRelativeTo(null);//Centramos la ventana
				
			calculadora();//Llamamos al metodo el calculadora.
		}
		
		//metodo en el que vamos a insertar botones,paneles y etiquetas con JButton,JPanel y JLabel.
		
		private void calculadora() {
			//Añadimos un panel en el que vamos a insertar elementos.
			JPanel panel = new JPanel();
			panel.setLayout(null);//Para poder modificar el diseño y poner elementos a nuestra manera.
			this.getContentPane().add(panel);//Agregamos panel
					
			panel.setBackground(Color.PINK);//Color de fondo
		
			//BOTONES
			JButton boton1 = new JButton();
			boton1.setBounds(100,200, 70, 50); //Lo posicionamos con x,y(primeros dos numeros) y lo moldeamos con width y height. 
			panel.add(boton1);//añadimos el boton al panel.
			boton1.setText("7");//Le ponemos texto al boton.
			//Y asi con cada boton.
			
			JButton boton2 = new JButton();
			boton2.setBounds(200,200, 70, 50);
			panel.add(boton2);
			boton2.setText("8");
			
			JButton boton3 = new JButton();
			boton3.setBounds(300,200, 70, 50);
			panel.add(boton3);
			boton3.setText("9");
			
			JButton boton4 = new JButton();
			boton4.setBounds(100,270, 70, 50);
			panel.add(boton4);
			boton4.setText("4");
			
			JButton boton5 = new JButton();
			boton5.setBounds(200,270, 70, 50);
			panel.add(boton5);
			boton5.setText("5");
			
			JButton boton6 = new JButton();
			boton6.setBounds(300,270, 70, 50);
			panel.add(boton6);
			boton6.setText("6");
			
			JButton boton7 = new JButton();
			boton7.setBounds(100,340, 70, 50);
			panel.add(boton7);
			boton7.setText("1");
			
			JButton boton8 = new JButton();
			boton8.setBounds(200,340, 70, 50);
			panel.add(boton8);
			boton8.setText("2");
			
			JButton boton9 = new JButton();
			boton9.setBounds(300,340, 70, 50);
			panel.add(boton9);
			boton9.setText("3");
			
			JButton boton0 = new JButton();
			boton0.setBounds(200, 400, 70,50);
			panel.add(boton0);
			boton0.setText("0");
			
			JButton suma = new JButton();
			suma.setBounds(400,200, 80, 100);
			panel.add(suma);
			suma.setText("+");
			
			JButton resta = new JButton();
			resta.setBounds(400,320, 80, 100);
			panel.add(resta);
			resta.setText("-");
			
			JButton mult = new JButton();
			mult.setBounds(5,200 , 80, 100);
			panel.add(mult);
			mult.setText("x");
			
			JButton div = new JButton();
			div.setBounds(5,320,80,100);
			panel.add(div);
			div.setText("/");
			
			JButton resultado = new JButton();
			resultado.setBounds(40,130 , 400, 50);
			panel.add(resultado);
			resultado.setText("=");
			
			//Aqui una etiqueta donde en un programa mas extenso deberia de ser un cuadro de texto en el que apareciesen las operaciones y el resultado.
			JLabel texto = new JLabel("Aqui deberian de salir los numeros de los botones pulsados");
			texto.setBounds(100,50,500,50);
			panel.add(texto);
		}
		
	}

